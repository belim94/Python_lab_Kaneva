# coding=utf-8
import pandas
from sklearn.decomposition import PCA
from numpy import corrcoef

import sys
sys.path.append("..")


# 1. Загрузите данные close_prices.csv. В этом файле приведены цены акций 30 компаний на закрытии торгов за каждый
# день периода.

data = pandas.read_csv('close_prices.csv').loc[:, 'AXP':]

# 2. На загруженных данных обучите преобразование PCA с числом компоненты равным 10. Скольких компонент хватит,
# чтобы объяснить 90% дисперсии?


PCA(n_components=10).fit(data.values)
disp = 0
comp = 0
for v in PCA(n_components=10).explained_variance_ratio_:
    comp += 1
    disp += v
    if disp >= 0.9:
        break
print('2) Components quantity: '+ comp)

# 3. Примените построенное преобразование к исходным данным и возьмите значения первой компоненты.

firComp = pandas.DataFrame(PCA(n_components=10).transform(data))[0]

# 4. Загрузите информацию об индексе Доу-Джонса из файла djia_index.csv. Чему равна корреляция Пирсона между первой
# компонентой и индексом Доу-Джонса?

idData = pandas.read_csv('djia_index.csv')['^DJI']
print('4) correl: '+ corrcoef(firComp, idData)[1, 0])

# 5. Какая компания имеет наибольший вес в первой компоненте? Укажите ее название с большой буквы.


topComp = pandas.Series(PCA(n_components=10).components_[0]).sort_values(ascending=False).head(1).index[0]
company = data.columns[topComp]
print('5) Top company by the first component'+ company)