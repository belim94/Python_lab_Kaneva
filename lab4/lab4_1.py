# coding=utf-8
import pandas
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import Ridge
from scipy.sparse import hstack

import sys
sys.path.append("..")

reader = pandas.read_csv('salary-train.csv')
def text_transform(text):
    text = text.map(lambda t: t.lower()).replace('[^a-zA-Z0-9]', ' ', regex=True)
    return text
textTrain = TfidfVectorizer(min_df=5).fit_transform(text_transform(reader['FullDescription']))
reader['LocationNormalized'].fillna('nan', inplace=True)
reader['ContractTime'].fillna('nan', inplace=True)
catTrain = DictVectorizer().fit_transform(reader[['LocationNormalized', 'ContractTime']].to_dict('records'))
ObjectsSigns = hstack([textTrain, catTrain])
Ridge(alpha=1).fit(ObjectsSigns, reader['SalaryNormalized'])
textTest = TfidfVectorizer(min_df=5).transform(text_transform(pandas.read_csv('salary-test-mini.csv')
                                                              ['FullDescription']))
catTest = DictVectorizer().transform(pandas.read_csv('salary-test-mini.csv')
                                     [['LocationNormalized', 'ContractTime']].to_dict('records'))
test = hstack([textTest, catTest])
print('Forecasts: {:0.2f} {:0.2f}'.format(Ridge(alpha=1).predict(test)[0], Ridge(alpha=1).predict(test)[1]))