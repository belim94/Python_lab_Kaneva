#!/usr/bin/env python
# -*- coding:utf-8 -*-
import pandas
import re
data = pandas.read_csv('titanic.csv', index_col='PassengerId')

pol = data['Sex'].value_counts()
print'1) мужчин: {} женщин: {}'.format(pol['male'], pol['female'])

alive = data['Survived'].value_counts()
alivePer = 100.0 * alive[1] / alive.sum()
print'2) процент выживших: {:0.2f}'.format(alivePer)

luxe = data['Pclass'].value_counts()
luxePer = 100.0 * luxe[1] / luxe.sum()
print'3) богачей: {:0.2f}'.format(luxePer)

age = data['Age']
print '4) средний возраст: {:0.2f} медиана: {:0.2f}'.format(age.mean(), age.median())

corel = data['SibSp'].corr(data['Parch'])
print '5) корреляция: {:0.2f}'.format(corel)

def clean_name(name):
    s = re.search('^[^,]+, (.*)', name)
    if s:
        name = s.group(1)

    s = re.search('\(([^)]+)\)', name)
    if s:
        name = s.group(1)

    name = re.sub('(Miss\. |Mrs\. |Ms\. )', '', name)

    name = name.split(' ')[0].replace('"', '')
    return name


names = data[data['Sex'] == 'female']['Name'].map(clean_name)
name_counts = names.value_counts()
print'6)', name_counts.head(1).index.values[0]