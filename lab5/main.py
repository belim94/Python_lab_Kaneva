# coding=utf-8
import pandas
import math
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.metrics import log_loss
import matplotlib.pyplot as plt

import sys
sys.path.append("..")

# 1. Загрузите выборку из файла gbm-data.csv с помощью pandas и преобразуйте ее в массив numpy (параметр values у
# датафрейма). В первой колонке файла с данными записано, была или нет реакция. Все остальные колонки (d1 - d1776)
# содержат различные характеристики молекулы, такие как размер, форма и т.д. Разбейте выборку на обучающую и тестовую,
# используя функцию train_test_split с параметрами test_size = 0.8 и random_state = 241.


reaction = pandas.read_csv('gbm-data.csv')['Activity'].values
characteristics = pandas.read_csv('gbm-data.csv').loc[:, 'D1':'D1776'].values
reactionTrain, reactionTest, characteristicsTrain, characteristicsTest = \
    train_test_split(reaction, characteristics, test_size=0.8, random_state=241)
print ('Reading and fragmentation Success...')

def sigmRefactoring(y_pred):
    return 1.0 / (1.0 + math.exp(-y_pred))
def log_loss_results(model, characteristics, reaction):
    result = []
    for pred in model.staged_decision_function(characteristics):
        result.append(log_loss(reaction, [sigmRefactoring(y_pred) for y_pred in pred]))
    return result

def plot_loss(learning_rate, test_loss, train_loss):
    plt.figure()
    plt.plot(test_loss, 'r', linewidth=2)
    plt.plot(train_loss, 'g', linewidth=2)
    plt.legend(['test', 'train'])
    plt.savefig('plots/rate_' + str(learning_rate) + '.png')
    metricValue = min(test_loss)
    metricIndex = test_loss.index(metricValue)
    return metricValue, metricIndex
def model_test(learning_rate):
    model = GradientBoostingClassifier(learning_rate=learning_rate, n_estimators=250, verbose=True, random_state=241)
    model.fit(characteristicsTrain, reactionTrain)
    train_loss = log_loss_results(model, characteristicsTrain, reactionTrain)
    test_loss = log_loss_results(model, characteristicsTest, reactionTest)
    return plot_loss(learning_rate, test_loss, train_loss)
minLossResults = {}
for learning_rate in [1, 0.5, 0.3, 0.2, 0.1]:
    minLossResults[learning_rate] = model_test(learning_rate)
print('3) overfitting')
minLossValue, minLossIndex = minLossResults[0.2]
print('4) min log-loss: {:0.2f} {}'.format(minLossValue, minLossIndex))
RandomForestClassifier(n_estimators=minLossIndex, random_state=241).fit(characteristicsTrain, reactionTrain)
ref_pred = RandomForestClassifier(n_estimators=minLossIndex, random_state=241).predict_proba(characteristicsTest)[:, 1]
test_loss = log_loss(reactionTest, ref_pred)
print_answer('test log-loss: ', test_loss)