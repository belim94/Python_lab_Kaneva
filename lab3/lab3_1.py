# coding=utf-8
import pandas
import numpy as np
from sklearn import datasets, grid_search
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import KFold

import sys
sys.path.append("..")

print ('1) Reading data...')
text = datasets.fetch_20newsgroups(subset='all', categories=['alt.atheism', 'sci.space']).data
classID = datasets.fetch_20newsgroups(subset='all', categories=['alt.atheism', 'sci.space']).target
print ('Success')
print ('2) TF-IDF count...')
TfidfVectorizer().fit_transform(text)
print ('Success')
grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(classID.size, n_folds=5, shuffle=True, random_state=241)
model = SVC(kernel='linear', random_state=241)
gs = grid_search.GridSearchCV(model, grid, scoring='accuracy', cv=cv)
gs.fit(TfidfVectorizer().transform(text), classID)
score = 0
C = 0
for attempt in gs.grid_scores_:
    if attempt.mean_validation_score > score:
        score = attempt.mean_validation_score
        C = attempt.parameters['C']
print ('3) is success')
model.fit(TfidfVectorizer().transform(text), classID)
print ('4) Training success')
words = TfidfVectorizer().get_feature_names()
coef = pandas.DataFrame(model.coef_.data, model.coef_.indices)
top_words = coef[0].map(lambda w: abs(w)).sort_values(ascending=False).head(10).index.map(lambda i: words[i])
print('1) Top words is: '+', '.join(top_words.sort()))