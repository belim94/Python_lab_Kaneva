# coding=utf-8
import pandas
import sklearn.metrics as metrics

import sys
sys.path.append("..")

reader = pandas.read_csv('classification.csv')
table = {'TP': (1, 1), 'FP': (0, 1), 'FN': (1, 0), 'TN': (0, 0)}
for name, res in table.iteritems():
    table[name] = len(reader[(reader['true'] == res[0]) & (reader['pred'] == res[1])])
print('2) TP={TP}, FP={FP}, FN={FN}, TN={TN}'.format(**table))
accuracy = metrics.accuracy_score(reader['true'], reader['pred'])
precision = metrics.precision_score(reader['true'], reader['pred'])
recall = metrics.recall_score(reader['true'], reader['pred'])
F = metrics.f1_score(reader['true'], reader['pred'])
print('3) Accuracy: {:0.2f}; Precision: {:0.2f}; Recall: {:0.2f}; F-measure{:0.2f}.'.format(accuracy, precision, recall, F))
scReader = pandas.read_csv('scores.csv')
scores = {}
for clf in scReader.columns[1:]:
    scores[clf] = metrics.roc_auc_score(scReader['true'], scReader[clf])
print('5) Higher AUC-ROC: '+ pandas.Series(scores).sort_values(ascending=False).head(1).index[0])
precisionScores = {}
for clf in scReader.columns[1:]:
    precisionCurve = metrics.precision_recall_curve(scReader['true'], scReader[clf])
    precisionCurveDataFrame = pandas.DataFrame({'precision': precisionCurve[0], 'recall': precisionCurve[1]})
    precisionScores[clf] = precisionCurveDataFrame[precisionCurveDataFrame['recall'] >= 0.7]['precision'].max()
print('6) Top precision: '+ pandas.Series(precisionScores).sort_values(ascending=False).head(1).index[0])