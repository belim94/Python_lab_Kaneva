# coding=utf-8
import pandas
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

import sys
sys.path.append("..")

print ('1) Reading data...')
perTrain = pandas.read_csv('perceptron-train.csv', header=None)[0]
priTrain = pandas.read_csv('perceptron-train.csv', header=None).loc[:, 1:]
perTest = pandas.read_csv('perceptron-test.csv', header=None)[0]
priTest = pandas.read_csv('perceptron-test.csv', header=None).loc[:, 1:]
print ('Success')
print ('2) Training...')
Perceptron(random_state=241).fit(priTrain, perTrain)
print ('Success')
print ('3) Count...')
accBefore = accuracy_score(perTest, Perceptron(random_state=241).predict(priTest))
print ('Success')
print ('4) Scaling...')
priTrainScaled = StandardScaler().fit_transform(priTrain)
priTestScaled = StandardScaler().transform(priTest)
print ('Success')
print ('5) New training...')
Perceptron(random_state=241).fit(priTrainScaled, perTrain)
accAfter = accuracy_score(perTest, Perceptron(random_state=241).predict(priTestScaled))
print ('Success')
print('Difference between accuracies is '+ accAfter - accBefore)