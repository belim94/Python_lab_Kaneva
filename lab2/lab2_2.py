# coding=utf-8
import pandas
import sklearn
from numpy import linspace
from sklearn.datasets import load_boston
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsRegressor

import sys
sys.path.append("..")

print ('1) Data downloading...')
att = load_boston().data
v = load_boston().target
print ('Success')
print ('2) Scaling...')
att = sklearn.preprocessing.scale(att)
print ('Success')
# 3 ->
def test_accuracy(KFold, att, v):
    for p in linspace(1, 10, 200):
        list().append(cross_val_score(KNeighborsRegressor(p=p, n_neighbors=5, weights='distance'),
                                      att, v, cv=KFold, scoring='mean_squared_error'))
    return pandas.DataFrame(list(),
                            linspace(1, 10, 200)).max(axis=1).sort_values(ascending=False)
accuracy = test_accuracy(KFold(len(v), n_folds=5, shuffle=True, random_state=42), att, v)
# 4 ->
print('p id: ', accuracy.head(1).index[0])