# coding=utf-8
import pandas
import sklearn
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsClassifier

import sys
sys.path.append("..")

print ('Point 2 is start...')
klas = pandas.read_csv('wine.data', header=None)[0]
att = pandas.read_csv('wine.data', header=None).loc[:, 1:]
print ('Success')
print ('Point 3 is start...')
KFold = KFold(len(klas), n_folds=5, shuffle=True, random_state=42)
print ('Success')
def test_accuracy(KFold, att, klas):
    for k in xrange(1, 51):
        list().append(cross_val_score(KNeighborsClassifier(n_neighbors=k),
                                      att, klas, cv=KFold, scoring='accuracy'))
    return pandas.DataFrame(list(), xrange(1, 51)).mean(axis=1).sort_values(ascending=False)
print ('1) k id=' + test_accuracy(KFold, att, klas).head(1).index[0])
print ('2) k val='+ test_accuracy(KFold, att, klas).head(1).values[0])
accuracy = test_accuracy(KFold, sklearn.preprocessing.scale(att), klas)
print ('3) post k id=', accuracy.head(1).index[0])
print ('4) post k val=', accuracy.head(1).values[0])